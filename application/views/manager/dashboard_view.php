<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		<?php if(!empty($site_name)){ echo $site_name; } ?> CATS DASHBOARD
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url(); ?>/manager"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Tribute</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            Aplikasi ini milik laboratorium ERP, dikembangan dengan dedikasi tinggi untuk mahasiswa Sistem Informasi
        </div><!-- /.box-body -->
    </div><!-- /.box -->
	<div class="callout callout-info">
    	<h4>Informasi</h4>
        <p>Selamat datang <b>Asisten</b> dilaman administratif Test Online</p>
    </div>
    <div class="box box-success box-solid">
		<div class="box-header with-border">
        	<h3 class="box-title">INFORMASI MENU CATS</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
        	<dl>
        		<dt>Data Modul</dt>
                <dd>
                	Kelompok Data Modul digunakan untuk pengelolaan modul, dan soal. Serta digunakan untuk mengatur file dengan memanfaatkan File Manager.
                	<ol>
                		<li><b>Modul</b>		: Tambah, Edit, Hapus Modul berdasarkan Praktikum</li>
                		<li><b>Soal</b>			: Tambah Soal dan Jawaban Berdasarkan Modul Praktikum</li>
                		<li><b>Import Soal</b>	: Tambah soal dengan import file excel</li>
                		<li><b>Daftar Soal</b>	: Melihat, Edit, Hapus Soal berdasarkan modul praktikum</li>
                		<li><b>File Manager</b>	: Tambah, Edit, Hapus File yang akan digunakan dalam soal</li>
                	</ol>
                </dd>
                <dt>Data Praktikan</dt>
                <dd>
                	Kelompok Data Praktikan digunakan untuk mengatur Praktikan, dan Group Praktikum.
                	<ol>
                		<li><b>Daftar Group Praktikum</b>	: Tambah, Edit, Hapus Grup / Shift Praktikum</li>
                		<li><b>Daftar Praktikan</b>			: Tambah, Edit, Hapus Data Praktikan Berdasarkan Grup Praktikum</li>
                		<li><b>Import Data Praktikan</b>	: Tambah Praktikan Dengan Import File Excel</li>
                	</ol>
                <dt>Data Tes</dt>
                <dd>
                	Kelompok Data Tes digunakan untuk mengatur Tes, mengevaluasi jawaban essay, dan melihat Hasil tes.
                	<ol>
                		<li><b>Tambah Tes</b>		: Tambah Tes dan Soal Berdasarkan Modul Praktikum</li>
                		<li><b>Daftar Tes</b>		: Edit, Hapus Tes dan Soal Berdasarkan Modul</li>
                		<li><b>Evaluasi Tes</b>		: Lihat, Tambah Nilai Essai</li>
                		<li><b>Hasil tes</b>		: Lihat Nilai, Edit Status Test Praktikan Berdasarkan Modul dan Test</li>
                        <li><b>Rekap Hasil Tes</b>	: Export Laporan Rekap Nilai Praktikan Berdasarkan Grup Praktikum</li>
                		<li><b>Token</b>			: Generate Token Akses Test</li>
                	</ol>
                </dd>
            </dl>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->